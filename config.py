#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import network


class Config:
    data_dir = 'data/names/'
    model_dir = 'models/'

    batch_size = 8
    num_epochs = 2
    show_interval = 500
    learning_rate = 0.0003

    cell = network.TANHCell
    hidden_size = 128
    num_layers = 2
