#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import torch
import torch.nn


class TANHCell(torch.nn.Module):
    def __init__(self, input_size: int, hidden_size: int, bias=True):
        super(TANHCell, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.bias = bias

        assert self.input_size > 0
        assert self.hidden_size > 0

        self.w_xh = torch.nn.Parameter(torch.randn((input_size, hidden_size), requires_grad=True))
        self.w_hh = torch.nn.Parameter(torch.randn((hidden_size, hidden_size), requires_grad=True))
        self.b_h = torch.nn.Parameter(torch.zeros(hidden_size, requires_grad=bias))

    def forward(self, x, hidden=None):
        if hidden is None:
            hidden = torch.zeros((x.size(0), self.hidden_size), requires_grad=False)

        hidden = torch.tanh(x.mm(self.w_xh) + hidden.mm(self.w_hh) + self.b_h)
        output = hidden
        return output, hidden


class GRUCell(torch.nn.Module):
    def __init__(self, input_size: int, hidden_size: int, bias=True):
        super(GRUCell, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.bias = bias

        assert self.input_size > 0
        assert self.hidden_size > 0

        self.w_xr = torch.nn.Parameter(torch.randn((input_size, hidden_size), requires_grad=True))
        self.w_xz = torch.nn.Parameter(torch.randn((input_size, hidden_size), requires_grad=True))
        self.w_xh = torch.nn.Parameter(torch.randn((input_size, hidden_size), requires_grad=True))
        self.w_hr = torch.nn.Parameter(torch.randn((hidden_size, hidden_size), requires_grad=True))
        self.w_hz = torch.nn.Parameter(torch.randn((hidden_size, hidden_size), requires_grad=True))
        self.w_hh = torch.nn.Parameter(torch.randn((hidden_size, hidden_size), requires_grad=True))
        self.b_r = torch.nn.Parameter(torch.zeros(hidden_size, requires_grad=bias))
        self.b_z = torch.nn.Parameter(torch.zeros(hidden_size, requires_grad=bias))
        self.b_h = torch.nn.Parameter(torch.zeros(hidden_size, requires_grad=bias))

    def forward(self, x, hidden=None):
        if hidden is None:
            hidden = torch.zeros((x.size(0), self.hidden_size), requires_grad=False)

        r = torch.sigmoid(x.mm(self.w_xr) + hidden.mm(self.w_hr) + self.b_r)
        z = torch.sigmoid(x.mm(self.w_xz) + hidden.mm(self.w_hz) + self.b_z)
        h = torch.tanh(x.mm(self.w_xh) + (r*hidden).mm(self.w_hh) + self.b_h)
        hidden = z * hidden + (1 - z) * h
        output = hidden
        return output, hidden


class LSTMCell(torch.nn.Module):
    def __init__(self, input_size, hidden_size, bias=True):
        super(LSTMCell, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.bias = bias

        assert self.input_size > 0
        assert self.hidden_size > 0

        self.w_xf = torch.nn.Parameter(torch.randn((input_size, hidden_size), requires_grad=True))
        self.w_xi = torch.nn.Parameter(torch.randn((input_size, hidden_size), requires_grad=True))
        self.w_xo = torch.nn.Parameter(torch.randn((input_size, hidden_size), requires_grad=True))
        self.w_xc = torch.nn.Parameter(torch.randn((input_size, hidden_size), requires_grad=True))
        self.w_hf = torch.nn.Parameter(torch.randn((hidden_size, hidden_size), requires_grad=True))
        self.w_hi = torch.nn.Parameter(torch.randn((hidden_size, hidden_size), requires_grad=True))
        self.w_ho = torch.nn.Parameter(torch.randn((hidden_size, hidden_size), requires_grad=True))
        self.w_hc = torch.nn.Parameter(torch.randn((hidden_size, hidden_size), requires_grad=True))
        self.b_f = torch.nn.Parameter(torch.zeros(hidden_size, requires_grad=bias))
        self.b_i = torch.nn.Parameter(torch.zeros(hidden_size, requires_grad=bias))
        self.b_o = torch.nn.Parameter(torch.zeros(hidden_size, requires_grad=bias))
        self.b_c = torch.nn.Parameter(torch.zeros(hidden_size, requires_grad=bias))

    def forward(self, x, hidden=None):
        if hidden is None:
            hidden = torch.zeros((x.size(0), self.hidden_size * 2), requires_grad=False)
        h, c = hidden[:, :self.hidden_size], hidden[:, self.hidden_size:]

        f = torch.sigmoid(x.mm(self.w_xf) + h.mm(self.w_hf) + self.b_f)
        i = torch.sigmoid(x.mm(self.w_xi) + h.mm(self.w_hi) + self.b_i)
        o = torch.sigmoid(x.mm(self.w_xo) + h.mm(self.w_ho) + self.b_o)
        c_hat = torch.tanh(x.mm(self.w_xc) + h.mm(self.w_hc) + self.b_c)
        c = f * c + i * c_hat
        h = o * torch.tanh(c)
        output = h
        return output, torch.cat((h, c), dim=1)


class RNN(torch.nn.Module):
    def __init__(self, cell_module, input_size: int, hidden_size: int, num_layers=1, bias=True, dropout=0, bidirectional=False):
        super(RNN, self).__init__()
        self.Cell = cell_module
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.bias = bias
        self.dropout = dropout

        assert self.input_size > 0
        assert self.hidden_size > 0
        assert self.num_layers > 0
        assert 0 <= self.dropout <= 1
        if dropout > 0:
            raise NotImplementedError
        if bidirectional != False:
            raise NotImplementedError

        self.layers = torch.nn.ModuleList([self.Cell(input_size, hidden_size, bias)])
        for i in range(1, num_layers):
            self.layers.append(self.Cell(hidden_size, hidden_size, bias))

    def forward(self, x, hx=None):
        if len(x.size()) == 1:
            x = x.view(1, -1)
        hidden_size = self.hidden_size * (2 if self.Cell is LSTMCell else 1)
        if hx is None:
            hx = torch.zeros((self.num_layers, x.size(0), hidden_size), device=x.device, requires_grad=False)
        hidden = torch.zeros((self.num_layers, x.size(0), hidden_size), device=x.device)

        for i in range(self.num_layers):
            if i > 0:
                output, hidden[i] = self.layers[i](output, hx[i])
            else:
                output, hidden[i] = self.layers[i](x, hx[i])
        return output, hidden


class NameClassifier(torch.nn.Module):
    def __init__(self, num_classes: int, cell_module, input_size, hidden_size, num_layers=1, bias=True):
        super(NameClassifier, self).__init__()
        self.n_class = num_classes
        self.rnn = RNN(cell_module=cell_module, input_size=input_size, hidden_size=hidden_size, num_layers=num_layers, bias=bias)
        self.fc = torch.nn.Linear(in_features=hidden_size, out_features=num_classes)
        self.softmax = torch.nn.LogSoftmax(dim=1)

    def forward(self, x):
        hidden = None
        for i in range(x.shape[0]):
            output, hidden = self.rnn(x[i], hidden)
        output = self.fc(output)
        output = self.softmax(output)
        return output


class CharGenerator(torch.nn.Module):
    def __init__(self, num_classes: int, cell_module, vocab_size, hidden_size, num_layers=1, bias=True):
        super(CharGenerator, self).__init__()
        self.n_class = num_classes
        self.vocab_size = vocab_size
        self.rnn = RNN(cell_module=cell_module, input_size=num_classes+vocab_size, hidden_size=hidden_size, num_layers=num_layers, bias=bias)
        self.fc = torch.nn.Linear(in_features=hidden_size, out_features=vocab_size)
        self.softmax = torch.nn.LogSoftmax(dim=1)

    def forward(self, cate, char, hidden=None):
        x = torch.cat((cate, char), dim=1)
        output, hidden = self.rnn(x, hidden)
        output = self.fc(output)
        output = self.softmax(output)
        return output, hidden

    def initHidden(self):
        return torch.zeros(1, 1, 128)
