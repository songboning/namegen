#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy
import os
import string
import torch
import torchvision  #如果没这个utils会找不到data
import unicodedata


class NameDataset(torch.utils.data.Dataset):
    """
    https://download.pytorch.org/tutorial/data.zip
    Args:
        root (string): Root directory of dataset where consist of some ``<language>.txt``,
            whom one line is a word from names.
    Note:
        all_categories should be consistent with self.classes
    """
    all_letters = string.ascii_letters + " .,;'-"
    all_categories = ['Portuguese', 'German', 'Dutch', 'English', 'Chinese', 'Irish', 'Spanish', 'Russian', 'Korean',
                      'Scottish', 'Vietnamese', 'Polish', 'Czech', 'Greek', 'French', 'Italian', 'Japanese', 'Arabic']

    def __init__(self, root='data/names', transform=None, target_transform=None):
        super(NameDataset, self).__init__()
        self.root = os.path.expanduser(root)
        self.transform = transform
        self.target_transform = target_transform
        self.data = []
        self.targets = []
        self.classes = []

        filenames = os.listdir(root)
        for i, filename in enumerate(filenames):
            self.classes.append(os.path.splitext(filename)[0])
            lines = open(os.path.join(root, filename), 'r')
            for line in lines:
                self.targets.append(i)
                self.data.append(self.unicode2ascii(line.strip()))

    def __getitem__(self, index):
        feature = self.data[index]
        target = self.targets[index]
        if self.transform:
            feature = self.transform(feature)
        if self.target_transform:
            target = self.target_transform(target)
        return feature, target

    def __len__(self):
        return len(self.targets)

    @classmethod
    def unicode2ascii(cls, s: str) -> str:
        return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn' and c in NameDataset.all_letters)


class NameTransform(object):
    def __init__(self, charset=None):
        if charset:
            self.charset = charset
        else:
            self.charset = NameDataset.all_letters

    def __call__(self, name: str):
        tensor = torch.zeros(len(name), len(self.charset))
        for i, letter in enumerate(name):
            tensor[i][self.letter2index(letter)] = 1
        return tensor

    def letter2index(self, letter):
        idx = self.charset.find(letter)
        assert idx >= 0
        return idx


class CategoryTransform(object):
    def __init__(self, categories=None):
        if categories:
            self.size = len(categories)
        else:
            self.size = len(NameDataset.all_categories)

    def __call__(self, category: int):
        one_hot = numpy.zeros(self.size)
        one_hot[category] = 1
        return one_hot


def name_collate(batch):
    batch = sorted(batch, key=lambda x: x[0].shape[0], reverse=True)
    max_length = batch[0][0].shape[0]
    data = torch.zeros((max_length, len(batch), batch[0][0].shape[1]))
    data[:, :, NameDataset.all_letters.find(' ')] = 1
    for i, item in enumerate(batch):
        for j in range(item[0].shape[0]):
            data[j][i] = item[0][j]
    targets = torch.LongTensor([x[1] for x in batch])
    return data, targets
