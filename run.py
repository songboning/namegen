#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import torch

from config import Config
import dataset
import network


def train_classifier(device=torch.device('cpu')):
    name_transform = dataset.NameTransform()
    name_dataset = dataset.NameDataset(root=Config.data_dir, transform=name_transform)
    batch_loader = torch.utils.data.DataLoader(name_dataset, batch_size=Config.batch_size, shuffle=True,
                                               collate_fn=dataset.name_collate)
    classifier = network.NameClassifier(len(name_dataset.classes), Config.cell, len(dataset.NameDataset.all_letters),
                                        hidden_size=Config.hidden_size, num_layers=Config.num_layers)
    classifier = classifier.to(device)
    criterion = torch.nn.NLLLoss()
    optimizer = torch.optim.Adam(classifier.parameters(), lr=Config.learning_rate)
    interval_loss = 0

    for i in range(Config.num_epochs):
        for j, (names, targets) in enumerate(batch_loader):
            names = names.to(device)
            targets = targets.to(device)
            outputs = classifier(names)
            loss = criterion(outputs, targets)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            interval_loss += loss

            if j % Config.show_interval == 0:
                correct = 0
                for k, target in enumerate(targets):
                    if outputs[k].argmax() == target:
                        correct += 1
                accuracy = correct / len(targets)
                print('[%d-%d]loss: %.2f,\tbatch accuracy: %.2f' % (i, j, interval_loss, accuracy))
                interval_loss = 0

    if Config.model_dir:
        torch.save(classifier.state_dict(), os.path.join(Config.model_dir, 'classifier_state.pt'))
    return classifier


def test_classifier(name: str, n_pred=3, classifier=None, device=torch.device('cpu')):
    if not classifier:
        if Config.model_dir:
            classifier = network.NameClassifier(len(dataset.NameDataset.all_categories), Config.cell,
                                                len(dataset.NameDataset.all_letters), hidden_size=Config.hidden_size,
                                                num_layers=Config.num_layers)
            classifier.load_state_dict(torch.load(os.path.join(Config.model_dir, 'classifier_state.pt')))
        else:
            raise ModuleNotFoundError
    classifier = classifier.to(device)
    classifier.eval()
    name_transform = dataset.NameTransform()
    name = dataset.NameDataset.unicode2ascii(name)
    name = name_transform(name)
    name, _ = dataset.name_collate([(name, 0)])
    name = name.to(device)
    output = classifier(name)
    topv, topi = output.topk(k=n_pred, dim=1, largest=True)
    predictions = []
    for i in range(n_pred):
        predictions.append((topv[0][i].item(), dataset.NameDataset.all_categories[topi[0][i]]))
        print(predictions[-1])
    return predictions


def train_generator(device=torch.device('cpu')):
    name_transform = dataset.NameTransform()
    cate_transform = dataset.CategoryTransform()
    name_dataset = dataset.NameDataset(root=Config.data_dir, transform=name_transform, target_transform=cate_transform)
    batch_loader = torch.utils.data.DataLoader(name_dataset, batch_size=Config.batch_size, shuffle=True,
                                               collate_fn=dataset.name_collate)
    generator = network.CharGenerator(len(name_dataset.classes), Config.cell, len(dataset.NameDataset.all_letters),
                                      hidden_size=Config.hidden_size, num_layers=Config.num_layers)
    generator = generator.to(device)
    criterion = torch.nn.NLLLoss()
    optimizer = torch.optim.Adam(generator.parameters(), lr=Config.learning_rate)

    for i in range(Config.num_epochs):
        for j, (names, categories) in enumerate(batch_loader):
            hidden = None
            targets = []
            for k in range(1, names.shape[0]):
                targets.append(torch.argmax(names[k], dim=1).numpy())
            targets.append([name_dataset.all_letters.find(' ')] * names.shape[1])
            targets = torch.LongTensor(targets)
            names = names.to(device)
            categories = categories.to(device, dtype=torch.float)
            targets = targets.to(device)
            loss = 0
            generator.zero_grad()
            for k in range(names.shape[0]):
                output, hidden = generator(categories, names[k], hidden)
                loss += criterion(output, targets[k])
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            if j % Config.show_interval == 0:
                print('[%d-%d] loss: %.2f' % (i, j, loss))

    if Config.model_dir:
        torch.save(generator.state_dict(), os.path.join(Config.model_dir, 'generator_state.pt'))
    return generator


def sample_generator(category: int, in_char: str, generator=None, max_len=10, device=torch.device('cpu')):
    assert len(in_char) == 1
    if not generator:
        if Config.model_dir:
            generator = network.CharGenerator(len(dataset.NameDataset.all_categories), Config.cell,
                                              len(dataset.NameDataset.all_letters), hidden_size=Config.hidden_size,
                                              num_layers=Config.num_layers)
            generator.load_state_dict(torch.load(os.path.join(Config.model_dir, 'generator_state.pt')))
        else:
            raise ModuleNotFoundError

    generator = generator.to(device)
    generator.eval()
    name_transform = dataset.NameTransform()
    cate_transform = dataset.CategoryTransform()
    category = torch.Tensor([cate_transform(category)]).to(device)
    in_char = dataset.NameDataset.unicode2ascii(in_char)
    name = ''
    hidden = None

    while len(name) < max_len and in_char != ' ':
        name += in_char
        x = name_transform(in_char).to(device)
        output, hidden = generator(category, x, hidden)
        in_char = dataset.NameDataset.all_letters[torch.argmax(output)]
    return name


def run():
    print("一个判断字母名字字段来自那种语言，或给出首字母生成指定语言名字的工具")
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print(device)
    while(True):
        print("清选择:")
        print("\t0. Exit")
        print("\t1. 训练分类器")
        print("\t2. 判断名字母语")
        print("\t3. 训练生成器")
        print("\t4. 生成名字")
        choice = input().strip()
        if choice == '0' or choice == 'q':
            break
        elif choice == '1':
            print("请在config.py中设定参数,按Enter继续,输入n取消")
            if input().strip() != 'n':
                train_classifier(device=device)
        elif choice == '2':
            print("请输入一个表示名字的单词:")
            name = input()
            test_classifier(name, device=device)
        elif choice == '3':
            print("请在config.py中设定参数,按Enter继续,输入n取消")
            if input().strip() != 'n':
                train_generator(device=device)
        elif choice == '4':
            print("请选择名字的母语:", dataset.NameDataset.all_categories)
            try:
                cate = dataset.NameDataset.all_categories.index(input().strip())
            except Exception:
                print(Exception)
                continue
            print("清输入一个大写字符串,表示每段名字的首字母")
            starts = input().strip()
            names = []
            for start in starts:
                assert ord('A') <= ord(start) <= ord('Z')
                name = sample_generator(category=cate, in_char=start, device=device)
                names.append(name)
            print(' '.join(names))
        else:
            print("Format Error!")
    print("End.")


run()